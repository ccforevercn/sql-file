## 数据的增删改查
#### 增(INSERT)
##### 批量添加数据
```
INSERT INTO tbl_name (title,age) VALUES ("标题1", 10), ("标题2", 20)
```
```
# 优先级设置
# LOW_PRIORITY 低 (其他高优先级的执行完后再执行，如果一直有高优先级的操作来执行就得一直等高优先级的操作执行完后才会执行)
# HIGH_PRIORITY 高 (其他sql执行完后立即执行)
# DELAYED 异步(先把当前sql放到内存中的等空闲的时候再执行。放入内存时并同时返回客户端完成(实际时没有执行完成的，不会返回插入的ID))。5.6已弃用,5.7忽略,计划未来删除
# IGNORE 当字段中有重复值(唯一值或者主键重复)时忽略继续插入其他数据
# PARTITION 指定分区插入那些数据
# VALUES 批量插入数据
# VALUE 插入一条数据
# ON DUPLICATE KEY UPDATE 插入的数据中有重复值(唯一值或者主键重复)时就会只修改参数后的字段(类似修改sql没有条件时)
# SELECT 查询其他表中数据插入到当前表内
```
#### 改(UPDATE)
#### 查(SELECT)
#### 删(DELETE)
# 查询表中的指定字段的数据
SELECT col_name FROM db_name.tbl_name;

# 查询表中满足条件的的指定字段的数据。运算符： =,!=,>,>=,<,<=。 
SELECT col_name FROM db_name.tbl_name WHERE col_name = value;
# 查询条件的指定多个参数 `field` in (value,value,value...),in参数内也可以是一条sql语句
SELECT col_name FROM db_name.tbl_name WHERE col_name in (value1,value2,value3...)

#  [log](https://dev.mysql.com/doc/refman/5.7/en/stored-programs-logging.html)