## mysql锁
### 共享锁和排它锁
#### share Lock(共享锁)
<mark>**事务中开启共享锁时,当前事务没有执行COMMIT(提交)/rollback(回滚)操作时,在其他会话中操作添加的共享锁的行/表时,只能执行查询操作。其他操作会被堵塞挂起,等事务完成后才会触发对应的修改操作。在其他事务中可是用共享锁且不可用排它锁。加锁方式:`SELECT ... LOCK IN SHARE MODE`**</mark>
| 共享锁会话 | 普通会话 | 其他共享锁会话 | 其他排它锁会话 |
| --- | --- | --- | --- |
| 开启事务: `BEGIN;` | | | |
| 开启行共享锁: `SELECT * FROM tbl_name WHERE id = 1 LOCK IN SHARE MODE;`(展示查询数据) | | | |
| | 查询被加锁的行: `SELCT * FROM tbl_name WHERE id = 1;` (成功) | 开启事务: `BEGIN;` |  开启事务: `BEGIN;` |
| | 修改被加锁的行: `UPDATA tbl_name SET title = "标题1-1" SET id = 1;` (等待锁释放) | 开启行共享锁: `SELECT * FROM tbl_name WHERE id = 1 LOCK IN SHARE MODE;`(展示查询数据) | 开启行排它锁: `SELECT * FROM tbl_name WHERE id = 1 FOR UPDATE; (报错)` |
| 提交事务: `COMMIT;`| 锁释放后提示修改完成 | | |

#### Eclusive Lock(排它锁)
<mark>**事务中开启排它锁时,当前事务没有执行COMMIT(提交)/rollback(回滚)操作时,只能执行查询操作。其他操作会被堵塞挂起,等事务完成后才会触发对应的修改操作。加锁方式:`SELECT ... FOR UPDATE`**</mark>

### 全局锁、表级锁、行级锁
#### 全局锁
<mark>**加锁后mysql中的所有数据库中的数据只能查询且不能修改和添加。加锁方式: `FLUSH TABLES WITH READ LOCK;`,释放锁方式: `UNLOCK TABLES;`**。</mark>
**注意: mysqldump备份时会先开启事务确保备份时的数据一致性**
| 全局锁会话 | 普通会话 |
| --- | --- |
| 开启全局锁: `FLUSH TABLES WITH READ LOCK;` | |
| | 查询: `select * from database.tbl_name`(展示查询数据) |
| | 插入: `insert into database.tbl_name(id,title,age) values(1,"标题1", 20),(2,"标题2", 30)` (挂起等待全局锁释放) |
| 释放全局锁: `UNLOCK TABLES;` | 锁释放后提示插入完成 |
#### 表级锁
##### 表锁
<mark>**整张表加锁,分为读锁和写锁。加锁方式:**</mark>
```bash
# 读锁
LOCK TABLES table_name READ;

# 加锁会话除了select(查询正常),其他修改操作都会拒绝(报错提示)
# 普通会话操作被加读锁的表除了select(查询正常),其他修改操作都会堵塞(挂起，等待释放读锁后执行)
# 释放读锁
UNLOCK TABLES;

# 写锁
LOCK TABLES table_name WRITE;

# 加锁会话执行修改和写都是正常执行
# 普通会话操作被加写锁的表都会堵塞(等待释放写锁后执行)
# 释放写锁
UNLOCK TABLES;
```
##### Metadata Lock(元数据锁/MDL)
<mark>**隐式添加,确保数据的正确性。分为读锁和写锁**</mark>
| 元数据锁类型 | 说明 |
| --- | --- |
| 读锁 | 执行CRUD操作时自动触发,每条CRUD语句相互不影响执行 |
| 写锁 | 修改表结构时会自动触发,执行修改表结构时其他读锁(CRUD语句)会堵塞或者已经再执行读锁(事务中的CRUD时且没有提交或者回滚事务)的语句时写锁(修改表结构)会堵塞等读锁释放后才会执行写锁内的语句 |
> MDL锁记录: mysql5.5版本中引入,直到mysql8.0版本中默认打开。在mysql5.7版本中需要先开启
>> 查看MDL锁记录状态: ``` SELECT * FROM `performance_schema`.`setup_instruments` WHERE NAME = 'wait/lock/metadata/sql/md' ```  
>> 开启MDL锁记录: ``` UPDATE `performance_schema`.`setup_instruments` SET ENABLED = 'YES',TIMED = 'YES' WHERE NAME = 'wait/lock/metadata/sql/mdl' ```
#### 行级锁