## 表语句
#### [创建表](https://dev.mysql.com/doc/refman/5.7/en/create-table.html)
##### 永久表创建
```
# 创建表时至少有一个字段
CREATE TABLE tbl_name (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL COMMENT '编号'
);
```
##### 临时表创建(TEMPORARY)
```
CREATE TEMPORARY TABLE tbl_name (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL COMMENT '编号'
);
```
##### 创建有外键约束的永久表
```
CREATE TABLE tbl_name (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL COMMENT '编号',
    title VARCHAR(32) NOT NULL COMMENT '标题' DEFAULT "标题"
);
CREATE TABLE tbl_name2 (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL COMMENT '编号',
    pid INT NOT NULL COMMENT '父级编号' DEFAULT 0,
    FOREIGN KEY (pid) REFERENCES tbl_name(id)
);
# 子表和父表中的外键约束字段需类型一致
# 创建外键约束时默认修改和删除操作都为RESTRICT
```
##### 永久表创建时修改表参数
```
CREATE TABLE tbl_name (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL COMMENT '编号'
) COMMENT "这是表注释" CHECKSUM 1;
```
##### 表分区
```
CREATE TABLE tbl_name (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL COMMENT '编号'
) PARTITION BY RANGE (id) PARTITIONS 3 (PARTITION p1 VALUES LESS THAN (10) COMMENT "ID小于10的存入当前分区", PARTITION p2 VALUES LESS THAN (100) COMMENT "ID小于100的存入当前分区", PARTITION p3 VALUES LESS THAN MAXVALUE COMMENT "ID大于100的存入当前分区");
# 根据id字段分为3个区,小于10的数据存入到p1中,大于10小于100的存入到p2内,大于100的存入到p3内
# 创建成功后在数据文件夹下的数据库目录下查看
# 分区字段必须是主键(如果分区不是ID时可加组合主键 PRIMARY KEY (id,分区的字段))

# 添加组合主键中的字段为分区字段
CREATE TABLE tbl_name (
    id INT AUTO_INCREMENT NOT NULL COMMENT '编号',
    title VARCHAR(32) NOT NULL COMMENT '标题' DEFAULT "标题",
    avater VARCHAR(32) NULL COMMENT '头像' DEFAULT "",
    age TINYINT(3) NOT NULL COMMENT '年龄' DEFAULT 18,
    PRIMARY KEY(id,age)
) PARTITION BY RANGE (age) PARTITIONS 3 (PARTITION p1 VALUES LESS THAN (18) COMMENT "未成年", PARTITION p2 VALUES LESS THAN (60) COMMENT "青年", PARTITION p3 VALUES LESS THAN MAXVALUE COMMENT "老年");
```
##### 表子分区
```
CREATE TABLE tbl_name (
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL COMMENT '编号'
) PARTITION BY RANGE (id) PARTITIONS 2 SUBPARTITION BY KEY(id) SUBPARTITIONS 2 (PARTITION p1 VALUES LESS THAN (1000) COMMENT "ID小于1000的存入当前分区" (SUBPARTITION p1s1 COMMENT "子分区1", SUBPARTITION p1s2 COMMENT "子分区2"), PARTITION p2 VALUES LESS THAN (1000000) COMMENT "ID小于100000的存入当前分区" (SUBPARTITION p2s1 COMMENT "子分区1", SUBPARTITION p2s2 COMMENT "子分区2"));
# 创建成功后在数据文件夹下的数据库目录下查看
```
##### 复制已存在的表字段信息到新表
```
CREATE TABLE new_tbl_name LIKE tbl_name
```
##### 复制已存在的表字段和数据到新表
```
CREATE TABLE new_tbl_name AS SELECT * FROM tbl_name
# * 代表复制所有字段和数据
# id,title 代码只复制id和title这两个字段和这两个字段的数据
```
##### 参数说明
###### 列参数
```
# DATA_TYPE 字段类型(常用的有INT,TINYINT,DOUBLE,FLOAT,DECIMAL,CHAR,VARCHAR,TEXT,LONGTEXT,TIMESTAMP)。 具体可参考:https://dev.mysql.com/doc/refman/5.7/en/data-types.html
# NULL 可以为空
# NOT NULL 不能为空
# DEFAULT 默认参数
# AUTO_INCREMENT 自动增长(第一条记录为1，第二条记录为2。在初始值上每次创建一条记录时累加1)
# COMMENT 注释
# PRIMARY KEY 主键约束(每个表中只能有一个主键)
# UNIQUE KEY 唯一值约束(多条记录中这个列值不能重复)
# COLLATE 排序规则
# COLUMN_FORMAT 数据存储格式,值分别为FIXED(固定宽度存储)、DYNAMIC(动态宽度存储)、DEFAULT(由列的数据类型确定的固定宽度或可变宽度存储。默认)。 一般不用设置，在NDB集群时设置
# STORAGE 数据存储位置,值分别为DISK(硬盘)、MEMORY(内存)。创建表时必须包含 TABLESPACE字句
```
###### 外键约束参数(FOREIGN KEY)
```
# FOREIGN KEY (col_name)  外键约束(col_name为当前表字段)
# REFERENCES tbl_name (col_name) 父表和父表中的字段

# MATCH FULL 外键字段的列必须都有值且不能为空,否则所有列的外键约束就会失效
# MATCH PARTIAL 外键字段值为空或者有值时都会匹配通过外键约束检查
# MATCH SIMPLE 默认值。外键字段的列可以为空,为空行的外键约束就会失效

# ON DELETE 父表执行删除操作时触发
# ON UPDATE 父表执行修改操作时触发

# RESTRICT 父表执行修改操作时拒绝修改。 先查询子表中是否有外键约束,有时拒接操作,没有时继续执行
# CASCADE 和父表中的操作一致，父表中删了当前行时触发子表删除当前行。父表中修改参数时触发子表当前行修改成对应的值
# SET NULL 父表中执行删除行或者修改行操作时，触发子表修改为NULL(子表中对应的外键字段必须申明为NULL)
# NO ACTION 父表执行修改操作时拒绝修改。先修改或删除父表中的数据,之后在查询子表中是否有外键约束的行,有时拒接操作。InnoDB引擎时和RESTRICT一致
# SET DEFAULT 非InnoDB引擎时可设置的参数。 父表中执行删除行或者修改行操作时，触发子表修改为默认值
```
###### 表参数
```
# AUTO_INCREMENT 主键自增值
# AVG_ROW_LENGTH 平均每行的长度
# DEFAULT CHARACTER SET 默认字符集
# DEFAULT COLLATE 默认排序规则
# CHECKSUM 数据一致性。 0关闭,1开启。 查看tbl_name的数据一致性 CHECKSUM TABLE tbl_name
# COMMENT 注释
# COMPRESSION 页面压缩，压缩方式：ZLIB、LZ4、NONE(关闭压缩)
# CONNECTION 单独设置表的连接方式。 'mysql://root@localhost/test/people'
# DATA DIRECTORY 数据目录(绝对地址)
# INDEX DIRECTORY 索引目录(绝对地址)
# DELAY_KEY_WRITE 延迟写入。仅适用于MyISAM引擎表,0关闭,1开启。数据修改时新数据保存到磁盘且索引保存在内存中,当关闭表时再把索引更新到磁盘中。如果意外情况时，再次启动mysql时需要同步表和索引。 
# ENCRYPTION 数据加密。 'Y'开启,'N'关闭
# ENGINE 引擎, InnoDB、MyISAM等引擎方式
# INSERT_METHOD 插入行设置。MERGE引擎时适用，NO(阻止插入)、FIRST(插入第一行)、LAST(插入最后一行)
# KEY_BLOCK_SIZE 索引快大小设置，适用于MyISAM引擎表
# MAX_ROWS 存储的最大行数
# MIN_ROWS 存储的最小行数
# PACK_KEYS 打包索引，适用于MyISAM。 0禁止打包、1(开启。更新速度慢,查询速度更开)、DEFAULT(仅打包CAHR、VARCHAR、BINARY、VARBINARY列)
# ROW_FORMAT 存储行的物理格式。DEFAULT、DYNAMIC、FIXED、COMPRESSED、REDUNDANT、COMPACT
# STATS_AUTO_RECALC 是否自动重新计算表的持久统计信息InnoDB。DEFAULT(由innodb_stats_auto_recalc配置决定)、0(关闭自动计算统计信息)、1(表中10%的数据更改时,自动计算统计信息)
# STATS_PERSISTENT 是否启用表的持久统计信息InnoDB。DEFAULT(由innodb_stats_persistent配置决定)、0(关闭)、1(开启)
# STATS_SAMPLE_PAGES 估计索引列的基数和其他统计信息
# TABLESPACE 再现有的通用表空间、每表文件表空间、系统表空间内创建表 TABLESPACE tablespace_name
# UNION 用于将一组相同的 MyISAM表作为一个整体进行访问。这仅适用于 MERGE表格
```
###### 表分区参数(InnoD、NDB)
```
# PARTITION BY [LINEAR] HASH 根据一个或多个字段进行HASH处理分区。(HASH:计算(字段值%分区数)后分区,整数类型字段。LINEAR HASH:线性HASH,先转为HASH类型后再计算后分区)
# PARTITION BY [LINEAR] KEY  根据一个或多个字段进行分区(类似HASH,没有字段类型限制且不支持有索引前缀的字段)主键>唯一字段(NOT NULL)>其他字段(不包含text、blob。text和blob有前缀)。 主键和唯一字段同时存在时
# PARTITION BY RANGE 范围分区。需要定义至少一个VALUES LESS THAN
# PARTITION BY RANGE COLUMNS 根据多个字段的范围分区。字段类型需为整数类型或者字符串类型
# PARTITION BY LIST 列表分区。根据一组值分区，需要定义至少一个VALUES IN
# PARTITIONS 分区数量
# SUBPARTITION BY [LINEAR] HASH 子分区
# SUBPARTITION BY [LINEAR] KEY 子分区
# PARTITION 每个分支设置
## VALUES LESS THAN
## VALUES IN
## [STORAGE] ENGINE 分区引擎
## COMMENT 备注
## DATA DIRECTORY 数据文件夹(绝对路径)
## INDEX DIRECTORY 索引文件夹(绝对路径)
## MAX_ROWS 最大行数
## MIN_ROWS 最小行数
## TABLESPACE 分区指定表空间(存储引擎必须一致)
# SUBPARTITION 每个子分支设置
## [STORAGE] ENGINE 子分区引擎
## COMMENT 子分区备注
## DATA DIRECTORY 子分区数据文件夹(绝对路径)
## INDEX DIRECTORY 子分区索引文件夹(绝对路径)
## MAX_ROWS 子分区最大行数
## MIN_ROWS 子分区最小行数
## TABLESPACE 子分区指定表空间(存储引擎必须一致)
```
###### 克隆表或复制表参数
```
# LIKE 复制指定表格式和字段到新表中 
# IGNORE 重复数据跳过(主键字段和唯一值字段)
# REPLACE 重复数据时插入新值删除旧值
# AS SELECT 复制指定表格式、字段、表数据到新表中AS后面为SELECT语句查询(*为复制所有字段)
```
#### [修改表](https://dev.mysql.com/doc/refman/5.7/en/alter-table.html)
##### 修改表配置
```
ALTER TABLE tbl_name COMMENT "修改注释";
# 修改参数和创建参数一致
```
##### 修改表名称
```
ALTER TABLE tbl_name RENAME new_tbl_name;
```
##### 添加字段
```
# 一个字段添加
ALTER TABLE tbl_name ADD age TINYINT NOT NULL COMMENT "年龄" DEFAULT 18 AFTER id;
# tbl_name表中的id字段之后添加age字段
# AFTER col_name col_name字段之后
# FIRST 添加到第一个列

# 批量添加字段
ALTER TABLE tbl_name ADD (avater VARCHAR(32) NULL COMMENT '头像' DEFAULT '', phone CHAR(11) NOT NULL COMMENT '电话' DEFAULT '');
```
##### 添加索引
```
# 单个字段索引
# 方法1
CREATE UNIQUE INDEX IDX_PHONE_UNIQUE USING BTREE ON tbl_name(phone) COMMENT "电话字段添加唯一索引";
# 方法2
ALTER TABLE tbl_name ADD UNIQUE INDEX IDX_PHONE_UNIQUE(phone) USING BTREE COMMENT "电话字段添加唯一索引";

# 多个字段索引
# 方法1
CREATE INDEX IDX_ID_AGE_KEY USING BTREE ON tbl_name(id,title) COMMENT "编号和年龄字段添加关键字索引";
# 方法2
ALTER TABLE tbl_name ADD INDEX IDX_ID_AGE_KEY(id,age) COMMENT "编号和年龄字段添加关键字索引";

# UNIQUE INDEX 唯一索引
# FULLTEXT INDEX 全文索引
# SPATIAL INDEX 空间索引
# KEY_BLOCK_SIZE 索引快大小设置
# BTREE 树索引存储类型
# HASH 哈希索引存储类型
# WITH PARSER 解析器。 适用FULLTEXT(全文索引)。 全文索引默认不支持中文分词,NGRAM(中文、日文、韩文等解析)
# COMMENT 注释
# ALGORITHM (INPLACE(直接更新。默认,支持并发)、COPY(创建新表并将原表逐行插入到新表,不允许并发))
# LOCK 读写锁。 DEFAULT(ALGORITHM设置时并发读取和写入,否则并发读取和单独写入)、NONE(并发读取和写入)、SHARED(并发读取且阻止写入)、EXCLUSIVE(单独读取和写入)
# 索引配置适用规则 https://dev.mysql.com/doc/refman/5.7/en/create-index.html#create-index-options
```
##### 修改索引名
```
ALTER TABLE tbl_name RENAME INDEX IDX_PHONE_UNIQUE TO NEW_IDX_PHONE_UNIQUE
```
##### 添加主键
```
ALTER TABLE tbl_name ADD PRIMARY KEY USING BTREE (age) COMMENT '设置age为主键'
# 一个表中需只有一个主键
```
##### 添加外键
```
ALTER TABLE tbl_name ADD FOREIGN KEY (pid) REFERENCES tbl_name2(id)
```
##### 字段约束(未生效)
```
# 约束age字段的值只能小于18
ALTER TABLE tbl_name ADD CHECK(age < 18)
```
##### 修改字段名称
```
ALTER TABLE tbl_name CHANGE age new_age TINYINT(3) FIRST
```
##### 删除字段
```
ALTER TABLE tbl_name DROP new_age
```
##### 删除索引
```
ALTER TABLE tbl_name DROP new_age
```
##### 删除主键
```
ALTER TABLE tbl_name DROP PRIMARY KEY
```
##### 删除外键
```
ALTER TABLE tbl_name DROP FOREIGN KEY 外键名称
```
##### 修改字段信息
```
ALTER TABLE tbl_name MODIFY title VARCHAR(64) NULL AFTER id
```
##### 行排序(MyISAM)
```
ALTER TABLE tbl_name ORDER BY id DESC
```
#### [删除表](https://dev.mysql.com/doc/refman/5.7/en/drop-table.html)
##### 删除永久表
```
DROP TABLE IF EXISTS tbl_name
```
##### 删除临时表
```
DROP TEMPORARY TABLE IF EXISTS tbl_name
```