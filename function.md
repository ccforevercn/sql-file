## [内置函数](https://dev.mysql.com/doc/refman/5.7/en/built-in-function-reference.html)
#### 常用函数
名称 | 参数 | 描述 | 例子 |
--- | --- | --- | --- 
MAX | [DISTINCT] expr | 查找多条记录中的最大值 | `SELECT MAX(price) FROM product`<br/>在产品表内查询最大的产品价格
MIN | [DISTINCT] expr | 查询多条记录中的最小值 | `SELECT MIN(price) FROM product`<br/>在产品表内查询最小的产品价格
COUNT | 1. expr<br/>2. DISTINCT expr[,expr...] | 1. 单个非空数量<br/>2. 多个非空数量 | 1. `SELECT COUNT(id) FROM product`<br/>在产品表中查询ID的非空总数量<br/>2. `SELECT COUNT(DISTINCT id,remark) FROM product`<br/>在产品表中查询ID和备注的非空总数量
AVG | [DISTINCT] expr | 平均值 | `SELECT AVG(price) FROM product`<br/>查询产品表中的平均价格
SUM | [DISTINCT] expr | 累加和 | `SELECT SUM(stock) FROM product`<br/>查询产品表中的库存总数
STD | expr | 总体标准差 | `SELECT STD(price) FROM product`<br/>查询产品表中的价格总体标准差<br/>**标准差：所有的产品价格距平均价格的之间的差**<br/>*==所有的产品价格的越接近于平均价格时标准差的值越小,比如10,11,12,13,14<br/>所有的产品价格越大于或越小于平均价时标准差值越大,比如10,20,30,40,50==*
GROUP_CONCAT | expr | 多行合并为一行展示 | ` SELECT GROUP_CONCAT(name) FROM product`<br/>在产品表中获取产品名称并合并为一行展示<br/> `GROUP_CONCAT(col_name SEPARATOR '|')`<br/>设置分隔符为\|，默认为,