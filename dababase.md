## 数据库语句
#### 创建数据库
```
CREATE DATABASE IF NOT EXISTS 数据库名称 数据库配置

# IF NOT EXISTS 判断数据库不存在时创建
# 数据库配置：
# CHARACTER SET utf8mb4 设置数据库默认字符集
# COLLATE utf8mb4_general_ci 设置数据库默认排序规则

# 创建cms数据库并设置默认字符集和默认排序规则
CREATE DATABASE IF NOT EXISTS cmd CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci
```
#### 修改数据库
```
ALTER DATABASE 数据库名称 数据库配置

# 数据库配置(数据库目录中的db.opt文件中,创建数据库时生成)
# CHARACTER SET utf8mb4 设置数据库默认字符集
# COLLATE utf8mb4_general_ci 设置数据库默认排序规则

# 修改cms数据库的默认字符集和默认排序规则
ALTER DATABASE IF NOT EXISTS cmd CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci
```
#### 删除数据库
```
DROP DATABASE IF EXISTS 数据库名称

# IF EXISTS 判断数据库存在时再删除
# 删除数据库时，数据库中的临时表不会被一起删除
# 删除cms数据库
DROP DATABASE IF EXISTS cms
```