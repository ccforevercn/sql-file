## 日志
| 日志类型 | 日志说明 | 使用场景 |
| --- | --- | --- |
| [Undo-log](https://dev.mysql.com/doc/refman/5.7/en/innodb-undo-logs.html) | 撤销日志 | 记录事务中执行前数据更新(增删改)的数据状态。**未完成事务时发生宕机或者回滚等情况后,重启后撤销事务操作** |
| [Redo-log](https://dev.mysql.com/doc/refman/5.7/en/innodb-redo-log.html) | 重做日志 | 记录事务中执行后数据更新(增删改)的数据状态。**完成事务(在内存中)后还没有刷盘时发生宕机等情况后,重启后恢复事务操作** |
| [bin log](https://dev.mysql.com/doc/refman/8.3/en/binary-log.html) | 二进制日志 | 记录数据更新或有可能数据更新的SQL语句。主要用于主从和数据恢复 |
| [Error-log](https://dev.mysql.com/doc/refman/5.7/en/error-log.html) | 错误日志 | 记录启动、关闭的时间和错误、警告、注释等诊断消息 |
| [Relay-log](https://dev.mysql.com/doc/refman/5.7/en/replica-logs-relaylog.html) | 中继日志 | 类似二进制日志,记录数据库更改的事件和索引文件(所有使用的中继日志文件的名称)组成 |
| [Query-Log](https://dev.mysql.com/doc/refman/5.7/en/query-log.html) | 查询日志 | 记录启动、关闭的信息和执行的每个SQL语句 |
| [Slow-log](https://dev.mysql.com/doc/refman/5.7/en/slow-query-log.html) | 慢查询日志 | 记录超过指定执行时间的每个SQL语句和执行时间等信息 |
| [DDL-log](https://dev.mysql.com/doc/refman/5.7/en/ddl-log.html) | DDL日志/元数据日志 | 记录表分区的数据定义语句生成的元数据操作 |

```
# innodb_flush_log_at_trx_commit 控制Redo-log写入策略,值为(0,1,2)
# 0: 写入日志缓存(内存) -> 提交 --> 写入文件系统缓存(内存) --> 写入日志文件(磁盘)。 因为保存到内存(mysqld线程每秒把日志缓存写入文件系统缓存后再慢慢写入磁盘)就提交,所以性能好，如果意外崩溃就会导致丢失数据
# 1: 写入日志缓存(内存) -> 写入日志文件(磁盘) -> 提交。 默认配置。因为要操作磁盘所以性能差但保证数据安全
# 2: 写入日志缓存(内存) -> 写入文件系统缓存(内存) -> 提交 --> 写入日志文件(磁盘)。 mysql级别发生崩溃等意外情况不会丢失数据(数据已经保存在文件系统缓存中),只有机器意外挂掉才会有数据丢失的风险

# 修改操作 -> 根据修改条件从磁盘中获取要修改的数据 -> 在内存中修改从磁盘中拿出的数据 -> 保存日志(提交事务) -> 修改磁盘数据
# 注意: 写日志是顺序写入,修改磁盘数据是随机(需要先找到位置后再)写入。

# dirty page(脏页): 根据修改条件从磁盘中拿到内存中并修改且还没有把修改后的数据保存到磁盘
# MTR: 一次原子性操作的过程(这个过程会被记录到redo-log中)
# LSN: 首次启用到当前MTR的大小
# checkpoint: 在脏页链表中查找最早修改的数据进行刷盘
```
#### 事务过程分析
- 从磁盘中读取旧数据
- 记录旧数据到undo-log日志文件(用于事务执行失败后回滚数据)
- 修改内存中的旧数据为新数据
- 记录新数据到redo-log日志文件准备提交事务(prepare)
- 记录binlog日志文件准备提交事务(prepare)
- 修改后的数据写入到磁盘
```
# 开启事务
START TRANSACTION; || BEGIN;
# 提交事务
COMMIT;
# 回滚事务
ROLLBACK
```
#### Undo-log(撤销日志)
###### 参数说明
| 名称 | 描述 | 参数 | 说明 |
| --- | --- | --- | --- |
| innodb_max_undo_log_size | 最大值 | 1G | 撤销日志文件在磁盘中占用的最大空间值,超过最大空间值时会就阶段 |
| innodb_rollback_segments | 回滚段数量 | 128 | 回滚段:撤销日志存储的空间。回滚段不能小于33个(系统表空间占用1个,临时表空间占用32个),最大128个。默认128
| innodb_undo_directory | 文件地址 | | |
#### redo-log(重做日志)
#### binlog(二进制日志)
#### Error-log(错误日志)
#### Relay-log(中继日志)
#### Query-log(查询日志)
#### Slow-log(慢查询日志)
| 名称 | 描述 | 参数 | 说明 |
| --- | --- | --- | --- |
| slow_query_log | 状态 | 0关闭、1开启 | 是否开启记录执行时间长的sql |
| long_query_time | 临界值(s)。默认10 | 10 | 超过设置的秒数后记录慢sql |
| slow_query_log_file | 文件地址 | /mysql/data/***.log | 指定慢日志保存的文件位置和名称 |
#### DDL-log(DDL日志)
```
FLOOR(7 + RAND() * 5)
```